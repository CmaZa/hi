package com.example.hi.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

/**
 * @Author lzx
 * @Create 2023/6/21
 * @Desc
 */

@Component
public class MongoUtil
{

    @Autowired
    MongoTemplate mongoTemplate1;
    @Autowired
    MongoTemplate mongoTemplate2;
    @Autowired
    MongoTemplate mongoTemplate3;
    @Autowired
    MongoTemplate mongoTemplate4;

    public MongoTemplate getMongoTemplate(String name)
    {
        MongoTemplate mongoTemplate = null;
        switch (name)
        {
            case "d1":
                mongoTemplate = mongoTemplate1;
                break;
            case "d2":
                mongoTemplate = mongoTemplate2;
                break;
            case "d3":
                mongoTemplate = mongoTemplate3;
                break;
            case "d4":
                mongoTemplate = mongoTemplate4;
                break;
            default:
                break;
        }
        return mongoTemplate;
    }
}
