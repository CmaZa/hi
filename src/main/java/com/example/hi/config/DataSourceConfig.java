package com.example.hi.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @Author lzx
 * @Create 2023/6/9
 * @Desc  数据源配置信息类
 */

@Configuration
public class DataSourceConfig
{
    
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.datasource0")
    public DataSource dataSource0()
    {
        return DruidDataSourceBuilder.create().build();
    }
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.datasource1")
    public DataSource dataSource1()
    {
        return DruidDataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.datasource2")
    public DataSource dataSource2()
    {
        return DruidDataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.datasource3")
    public DataSource dataSource3()
    {
        return DruidDataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.datasource4")
    public DataSource dataSource4()
    {
        return DruidDataSourceBuilder.create().build();
    }
}
