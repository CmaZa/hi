package com.example.hi.config;


import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;


/**
 * mongoDb数据源配置类
 */
@Configuration
public class MongoConfig
{


    @Value("${mongoConfig.mongo1.uri}")
    private String mongo1Uri;

    @Value("${mongoConfig.mongo2.uri}")
    private String mongo2Uri;

    @Value("${mongoConfig.mongo3.uri}")
    private String mongo3Uri;

    @Value("${mongoConfig.mongo4.uri}")
    private String mongo4Uri;

    @Bean(name = "mongoTemplate1")
    public MongoTemplate mongoTemplate1() {
        MongoClient mongoClient = MongoClients.create(mongo1Uri);
        MongoDatabase mongoDatabase = mongoClient.getDatabase("d1");
        return new MongoTemplate(mongoClient, mongoDatabase.getName());
    }

    @Bean(name = "mongoTemplate2")
    public MongoTemplate mongoTemplate2() {
        MongoClient mongoClient = MongoClients.create(mongo2Uri);
        MongoDatabase mongoDatabase = mongoClient.getDatabase("d1");
        return new MongoTemplate(mongoClient, mongoDatabase.getName());
    }

    @Bean(name = "mongoTemplate3")
    public MongoTemplate mongoTemplate3() {
        MongoClient   mongoClient   = MongoClients.create(mongo3Uri);
        MongoDatabase mongoDatabase = mongoClient.getDatabase("d2");
        return new MongoTemplate(mongoClient, mongoDatabase.getName());
    }

    @Bean(name = "mongoTemplate4")
    public MongoTemplate mongoTemplate4() {
        MongoClient   mongoClient   = MongoClients.create(mongo4Uri);
        MongoDatabase mongoDatabase = mongoClient.getDatabase("d3");
        return new MongoTemplate(mongoClient, mongoDatabase.getName());
    }

    @Bean
    public GridFsTemplate gridFsTemplate(@Qualifier("mongoTemplate1") MongoTemplate mongoTemplate) {
        return new GridFsTemplate(mongoTemplate.getMongoDbFactory(), mongoTemplate.getConverter());
    }
}
