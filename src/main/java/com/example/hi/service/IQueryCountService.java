package com.example.hi.service;

import com.example.hi.entity.QueryVo;

/**
 * @Author lzx
 * @Create 2023/6/27
 * @Desc 统一查询接口
 */

public interface IQueryCountService

{
    public Object queryByVo(QueryVo queryVo);

}
