package com.example.hi.service.impl;

import cn.hutool.json.JSONUtil;
import com.example.hi.dataSource.DynamicDataSource;
import com.example.hi.entity.ParamsResult;
import com.example.hi.entity.QueryVo;
import com.example.hi.mapper.QueryMapper;
import com.example.hi.service.IQueryCountService;
import com.example.hi.util.MongoUtil;
import com.example.hi.util.RedisUtil;
import com.mongodb.DBObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import javax.swing.text.Document;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @Author lzx
 * @Create 2023/6/27
 * @Desc
 */
@Service
public class QueryCountServiceImpl implements IQueryCountService
{
    @Autowired
    MongoUtil mongoUtil;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    QueryMapper queryMapper;


    @Override
    public String queryByVo(QueryVo queryVo)
    {
        //仪表盘code
        String code = queryVo.getIndicatorCode();
        //指标code
        String zbs = queryVo.getZb();

        //根据仪表盘code查询对应的数据源信息
        DynamicDataSource.name.set("d0");
        ParamsResult result = queryMapper.executeQuery(code);

        //数据库类型
        String dbType = result.getSourceLx();
        //数据库别名
        String dbName = result.getParam();
        //该仪表盘对应的数据库表
        String tbName = result.getTabname();


        //根据数据库类型以及数据源信息动态的处理数据
        switch (dbType)
        {
            case "mysql":
                DynamicDataSource.name.set(dbName);
                return JSONUtil.toJsonStr(queryMapper.getSelectedColumnsFromTable(zbs, tbName));
            case "redis":
                //TODO
                break;
//                return  redisUtil.redisTemplate(dbName).opsForValue().get(tbName);
            case "mongo":
                MongoTemplate mongoTemplate = mongoUtil.getMongoTemplate(dbName);
                Criteria criteria;
                String[] fieldArray = zbs.split(",");
                Query query = new Query();
                for (String field : fieldArray)
                {
                    query.fields().include(field.trim());
                }
                List<Document> mongoResult = mongoTemplate.find(query, Document.class, tbName);
                return JSONUtil.toJsonStr(mongoResult);
            default:
                break;

        }
        return null;
    }

}
