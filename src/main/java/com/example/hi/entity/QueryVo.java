package com.example.hi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author lzx
 * @Create 2023/6/21
 * @Desc 前端传递的json映射对象
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QueryVo
{
    String indicatorCode;   //指标归属表的code
    String zb;              //需要的指标
}
