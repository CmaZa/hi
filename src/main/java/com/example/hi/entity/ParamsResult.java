package com.example.hi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author lzx
 * @Create 2023/6/30
 * @Desc  根据code查询的数据源信息参数
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParamsResult
{
    private String tabname;   //数据指标表对应的数据库中的一张表
    private String sourceLx;  //数据库类型
    private String param;     //数据库别名
}
