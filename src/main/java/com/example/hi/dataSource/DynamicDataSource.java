package com.example.hi.dataSource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @Author lzx
 * @Create 2023/6/9
 * @Desc 动态数据源配置类
 */

@Component
@Primary
public class DynamicDataSource extends AbstractRoutingDataSource
{
    //当前数据源的标识  使用ThreadLocal避免线程安全问题
    public static ThreadLocal<String> name = new ThreadLocal<>();

    @Autowired
    DataSource dataSource0;

    @Autowired
    DataSource dataSource1;

    @Autowired
    DataSource dataSource2;

    @Autowired
    DataSource dataSource3;

    @Autowired
    DataSource dataSource4;


    //返回当前数据源标识
    @Override
    protected Object determineCurrentLookupKey()
    {
        return name.get();
    }

    @Override
    public void afterPropertiesSet()
    {
        Map<Object, Object> targetDataSource = new HashMap<>();
        targetDataSource.put("d0", dataSource0);
        targetDataSource.put("d1", dataSource1);
        targetDataSource.put("d2", dataSource2);
        targetDataSource.put("d3", dataSource3);
        targetDataSource.put("d4", dataSource4);
        super.setTargetDataSources(targetDataSource);
        //设置默认的数据源
        super.setDefaultTargetDataSource(dataSource1);
        super.afterPropertiesSet();
    }
}
