package com.example.hi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@MapperScan("com.example.hi.mapper")
public class HiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HiApplication.class, args);
        System.out.println("测试工程启动成功！");

    }

}
