package com.example.hi.controller;

import com.example.hi.entity.QueryVo;
import com.example.hi.service.IQueryCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author lzx
 * @Create 2023/6/8
 * @Desc 统一查询接口
 */
@RestController
@RequestMapping("/sys")
public class HiController
{
    @Autowired
    IQueryCountService queryCountService;

    /**
     * 统一查询接口
     *
     * @param queryVos json参数映射对象
     * @return json字符串
     */
    @PostMapping("/query")
    public Object query(@RequestBody QueryVo queryVos)
    {
        return queryCountService.queryByVo(queryVos);
    }

}
