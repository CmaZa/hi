package com.example.hi.mapper;

import com.example.hi.entity.ParamsResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @Author lzx
 * @Create 2023/6/9
 * @Desc
 */


@Mapper
public interface QueryMapper
{
    @Select("SELECT ${columns} FROM ${tableName}")
    List<Map<String, Object>> getSelectedColumnsFromTable(
            @Param("columns") String columns,
            @Param("tableName") String tableName
    );

    @Select("SELECT tabname,sourceLx,param from indicator,sourcetable where indicator.code = ${code} and indicator.sourceCode = sourcetable.code")
    ParamsResult executeQuery(@Param("code") String code);


}

