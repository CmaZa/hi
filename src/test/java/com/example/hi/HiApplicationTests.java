package com.example.hi;

import cn.hutool.json.JSONUtil;
import com.example.hi.dataSource.DynamicDataSource;
import com.example.hi.entity.ParamsResult;
import com.example.hi.entity.QueryVo;
import com.example.hi.mapper.QueryMapper;
import com.example.hi.util.MongoUtil;
import com.example.hi.util.RedisUtil;
import com.mongodb.DBObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;


import javax.swing.text.Document;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@SpringBootTest
class HiApplicationTests
{
    @Autowired
    @Qualifier("mongoTemplate1")
    MongoTemplate mongoTemplate;

    @Test
    void contextLoads()
    {
        Query query = new Query(Criteria.where("age").gt(20));
        System.out.println(mongoTemplate.count(query, DBObject.class, "emp"));

    }

    @Autowired
    MongoUtil mongoUtil;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    QueryMapper queryMapper;


    @Test
    public void queryByVo()
    {
        QueryVo queryVo = new QueryVo("001", "year,man");

        String code = queryVo.getIndicatorCode();
        String zbs  = queryVo.getZb();


        //TODO 根据code查询配置表 获取bdType，param= dbName，tbName 字段
        DynamicDataSource.name.set("d0");
        ParamsResult result = queryMapper.executeQuery(code);

        String dbType = result.getSourceLx();
        String dbName = result.getParam();
        String tbName = result.getTabname();


        switch (dbType)
        {
            case "mysql":
                DynamicDataSource.name.set(dbName);
                System.out.println(JSONUtil.toJsonStr(queryMapper.getSelectedColumnsFromTable(zbs, tbName)));
            case "redis":
                //TODO
                break;
//                return  redisUtil.redisTemplate(dbName).opsForValue().get(tbName);
            case "mongo":
                MongoTemplate mongoTemplate = mongoUtil.getMongoTemplate(dbName);
                Criteria criteria;
                String[] fieldArray = zbs.split(",");
                Query query = new Query();
                for (String field : fieldArray)
                {
                    query.fields().include(field.trim());
                }
                List<javax.swing.text.Document> mongoResult = mongoTemplate.find(query, Document.class, tbName);
                System.out.println(JSONUtil.toJsonStr(mongoResult));
            default:
                break;

        }
    }

}
