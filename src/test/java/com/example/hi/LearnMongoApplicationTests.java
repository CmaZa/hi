package com.example.hi;

import com.mongodb.DBObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

/**
 * @Author lzx
 * @Create 2023/6/16
 * @Desc
 */
@SpringBootTest
public class LearnMongoApplicationTests
{
    @Autowired
    @Qualifier("mongoTemplate2")
    MongoTemplate mongoTemplate;

    @Test
    public void noNameQuery()
    {
        Query query = new Query();
        query.addCriteria(Criteria.where("age").gt(20));

        List<DBObject> all = mongoTemplate.find(query, DBObject.class, "emp");

        all.forEach(System.out::println);

    }
}
