package com.example.hi;


import cn.hutool.json.JSONUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @Author lzx
 * @Create 2023/6/16
 * @Desc
 */

public class MongoTemplateTests extends LearnMongoApplicationTests
{

    @Autowired
    @Qualifier("mongoTemplate2")
    MongoTemplate mongoTemplate;


    /**
     * 创建
     */
    @Test
    public void createCollection()
    {
        if (mongoTemplate.collectionExists("emp"))
        {
            mongoTemplate.dropCollection("emp");
            System.out.println("drop success");
        }
        mongoTemplate.createCollection("emp");
        System.out.println("create success");

    }


    /**
     * 插入
     */
    @Test
    public void insert()
    {
//        Employee lzx = new Employee(1, "lzx", 24, 10000.00, new Date());

//        mongoTemplate.insert(lzx);
//        mongoTemplate.save(lzx);

        List<Employee> list = Arrays.asList(
                new Employee(1, "lzx", 24, 10000.00, new Date()),
                new Employee(2, "zhangsan", 23, 5600.12, new Date()),
                new Employee(3, "lisi", 23, 5600.12, new Date()),
                new Employee(4, "wangwu", 23, 5600.12, new Date())
        );
        mongoTemplate.insert(list, Employee.class);
    }

    @Test
    public void query()
    {
        List<Employee> all = mongoTemplate.findAll(Employee.class);
        all.forEach(System.out::println);

//        List<Employee> list = mongoTemplate.find(new Query(Criteria.where("salary").gte(2000)), Employee.class);
//        list.forEach(System.out::println);

//
//        System.out.println(mongoTemplate.findById(1, Employee.class));
    }

    @Test
    public void queryByJson()
    {
        Employee zhangsan = new Employee();
        zhangsan.setId(2);
        zhangsan.setName("zhangsan");
        zhangsan.setAge(23);


        String json = JSONUtil.toJsonStr(zhangsan);
        System.out.println("================================================");
        System.out.println(JSONUtil.toJsonStr(zhangsan));

        mongoTemplate.find(new BasicQuery(json), Employee.class).forEach(System.out::println);

    }


}
